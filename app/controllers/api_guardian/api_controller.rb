module ApiGuardian
  class ApiController < ActionController::API
    include ::Pundit
    include ApiGuardian::Concerns::ApiErrors::Handler
    include ApiGuardian::Concerns::ApiRequest::Validator

    before_action :doorkeeper_authorize!
    before_action :set_current_user
    before_action :prep_response
    # before_action :validate_api_request
    # before_action :find_and_authorize_resource, except: [:index, :new, :create]
    # after_action :verify_policy_scoped, only: :index
    # after_action :verify_authorized, except: [:index]
    append_before_action :set_current_request

    rescue_from Exception, with: :api_error_handler

    attr_reader :current_user

    # def index
    #   @resources = should_paginate? ? resource_store.paginate(page_params[:number], page_params[:size]) : resource_store.all
    #   render json: @resources, include: includes
    # end
    def process_run
      result = run operation# do |result|

      raise result[:exception] unless result.success?

      ApiGuardian.logger.debug "Process_run===========: #{resource_serailizer}"
      
      if result[:model].is_a? Enumerable
        render json: result[:model], 
              include: includes, 
              each_serailizer: resource_serailizer
      else
        render json: result[:model], 
               include: result[:includes],
               serializer: resource_serailizer
      end
    end

    def index
      process_run
    end

    def show
      process_run
    end

    def create
      process_run
    end

    def update
      process_run
    end

    def destroy
      process_run
    end

    def append_info_to_payload(payload)
      super
      payload[:host] = request.host
    end

    protected

    def find_and_authorize_resource
      @resource = resource_store.find(params[:id])
      authorize @resource
    end

    def resource_store
      @resource_store ||= find_and_init_store
    end

    def resource_name
      @resource_name ||= controller_name.classify
    end

    def resource_class
      @resource_class ||= find_resource_class.constantize
    end

    # def resource_policy
    #   @resource_policy ||= action_name == 'index' ? policy_scope(resource_class) : nil
    # end

    def resources_operation
      @resources_operation ||= find_operation_calss
    end

    def resource_policy
      @resource_policy ||= find_policy_class
      # @resource_policy ||= action_name == 'index' ? policy_scope(resource_class) : nil
    end

    def resource_contract
      @resource_contract ||= find_resource_contract
      # @resource_policy ||= action_name == 'index' ? policy_scope(resource_class) : nil
    end

    def resource_serailizer
      @resource_serailizer ||= find_resource_serailizer.constantize
    end


    # :nocov:
    def includes
      []
    end
    # :nocov:

    def create_resource_params
      params.require(:data).require(:attributes).permit(create_params)
    end

    def update_resource_params
      params.require(:data).require(:attributes).permit(update_params)
    end

    # :nocov:
    def create_params
      []
    end
    # :nocov:

    def page_params
      params.fetch(:page, number: 1, size: 25)
    end

    # :nocov:
    def update_params
      []
    end
    # :nocov:

    def set_policy(new_policy = nil)
      @resource_policy = new_policy
    end

    def should_paginate?
      params[:paginate] != 'false'
    end

    private

    def set_current_user

      @current_user = if doorkeeper_token && !doorkeeper_token.resource_owner_id.present?
                        ApiGuardian.configuration.user_class.find(doorkeeper_token.application.owner_id)
                      else
                        ApiGuardian.configuration.user_class.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
                      end
      # @current_user = ApiGuardian.configuration.user_class.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
      ApiGuardian.current_user = @current_user
    end

    def prep_response
      response.headers['Content-Type'] = 'application/vnd.api+json'
    end

    def set_current_request
      ApiGuardian.current_request = request
    end

    def find_and_init_store
      store = nil

      # Check for app-specfic store
      if ApiGuardian.class_exists?(resource_name + 'Store')
        store = resource_name + 'Store'
      end

      # Check for ApiGuardian Store
      unless store
        if ApiGuardian.class_exists?('ApiGuardian::Stores::' + resource_name + 'Store')
          store = 'ApiGuardian::Stores::' + resource_name + 'Store'
        end
      end

      return store.constantize.new(resource_policy) if store

      fail ApiGuardian::Errors::ResourceStoreMissing, 'Could not find a resource store ' \
           "for #{resource_name}. Have you created one? You can override `#resource_store` " \
           'in your controller in order to set it up specifically.'
    end

    def find_resource_class
      if ApiGuardian.class_exists?(resource_name)
        return resource_name.constantize
      elsif ApiGuardian.configuration.respond_to? "#{resource_name.downcase}_class"
        return ApiGuardian.configuration.send("#{resource_name.downcase}_class")
      else
        fail ApiGuardian::Errors::ResourceClassMissing, 'Could not find a resource class (model) ' \
             "for #{resource_name}. Have you created one?"
      end
    end

    def _run_options(options)
      # options.merge(current_user: current_user,
      #   "policy.default.eval" => resource_policy,
      #   'contract.default.class' => contract)

      options.merge(current_user: current_user,
                    "policy.default.eval" => resource_policy,
                    'contract.default.class' => resource_contract,
                    request_ip: request.remote_ip)
    end

    def find_operation_calss
      action = params[:action]
      kclass = self.class.name.gsub(/Controller/, "::Operations::#{action.camelize}")
      if ApiGuardian.class_exists?(kclass)
        return kclass.safe_constantize
      else
        fail ApiGuardian::Errors::OperationClassMissing, 'Could not find a operation class ' \
             "for #{kclass}. Have you created one?"
      end
    end

    def find_policy_class
      action = params[:action]
      kclass = self.class.name.gsub(/Controller/, "::Policy")

      if ApiGuardian.class_exists?(kclass)
        return Trailblazer::Operation::Policy::Pundit.build(kclass.safe_constantize, "#{action}?".to_sym)
      else
        fail ApiGuardian::Errors::PolicyClassMissing, 'Could not find a policy class ' \
             "for #{kclass}. Have you created one?"
      end
    end

    def find_resource_contract
      action = params[:action]
      kclass = self.class.name.gsub(/Controller/, "::Contracts::#{action.camelize}")

      return kclass.safe_constantize
    end

    def find_resource_serailizer
      action = params[:action]
      kclass = self.class.name.gsub(/Controller/, "::Serializers::#{action.camelize}Serializer")
       
      if ApiGuardian.class_exists?(kclass)
        return kclass
      else
        fail ApiGuardian::Errors::SerializerClassMissing, 'Could not find a serializer class ' \
             "for #{kclass}. Have you created one?"
      end
    end

    def operation      
      @operation ||= resources_operation
    end
  end
end
