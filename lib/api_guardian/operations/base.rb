module ApiGuardian
  module Operations
    class Base < Trailblazer::Operation
      include ApiGuardian::Concerns::ApiErrors::Handler

      def process_policy(options, params:, **)
        policy = options['policy.default.eval']

        unless policy.present?        
          fail ApiGuardian::Errors::PolicyClassMissing, 'Could not find a policy class ' \
               "for #{kclass}. Have you created one?"
        end
        
        result = policy.call(options)
        options[:policy] = result['policy']
        options['result.policy.default'] = result
      
        result.success?
      end

      private
      def exception(exception, options)
        Rails.logger.error "======== exception #{exception}"  
        options[:exception] = exception
        options[:errors] = exception.message
      end

      def error!(options, params:, **)
        policy = options['result.policy.default']
        contract = options['result.contract.default']
        model = options[:model]
        

        Rails.logger.error "======== error! #{options.inspect}"  

        if policy.present? && policy.try(:success?) == false
          options[:errors] = '권한이 없습니다'
          raise ApiGuardian::Errors::PolicyError
        end

        if contract.present? && contract.try(:errors).try(:blank?) == false
          options[:errors] = contract.errors
          raise ApiGuardian::Errors::RecordInvalid.new(contract)
        end

        if model.present? && model.try(:errors).try(:blank?) == false
          options[:errors] = model.errors
          raise ActiveRecord::RecordInvalid.new(model)
        end       
      end

      def response_model(action, attributes)
        kclass = self.class.name.gsub("::Operations::#{action.capitalize}", "::Model::#{action.capitalize}::Response")

        if ApiGuardian.class_exists?(kclass)
          return  kclass.safe_constantize.new(attributes)
        else
          fail ApiGuardian::Errors::ResponseModelClassMissing, 'Could not find a policy class ' \
               "for #{kclass}. Have you created one?"
        end        
      end
    end
  end
end
